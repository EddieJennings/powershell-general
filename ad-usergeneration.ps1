<#
.Description
Interactive script creating test accounts in AD

.Notes
	Author:  Eddie Jennings, Jr.
#>

# UI Variables

$standardBackground = "Black"
$standardText = "Green"

# Create functions

function DetermineNumberOfUsersToCreate {
    do {
        Write-Host "`nEnter the total number of users to create, then press ENTER." -BackgroundColor $standardBackground -ForegroundColor $standardText
        [int]$numberOfUsers = Read-Host

        if ( ($numberOfUsers.GetTypeCode() -eq "Int32") -and ($numberOfUsers -gt 0) ) {
            $isDataValid = $true
        }
            else {
                Write-Warning -Message "Invalid data.  Please enter a number greater than zero."
                $isDataValid = $false
            }
    } while ( -not $isDataValid )

    $numberOfUsers
}

function UserPrefixCreationInstructions {
    Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "#             User Prefix Creation             #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# The user prefix is the word part of the      #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# username.  For example, if your user prefix  #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# is 'eddie' and you create two users, the     #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# created users will be eddie1 and eddie2.     #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText
    
    Write-Host "`nEnter a string of letters, then press ENTER." -BackgroundColor $standardBackground -ForegroundColor $standardText
}

function UserPrefixCreationChoice {
    
    do {
        UserPrefixCreationInstructions
        [string]$userPrefix = Read-Host
        
        # Is it a string?
        if ( $userPrefix.GetTypeCode() -eq 'String' ) {
            $isString = $true
        } else {
            $isString = $false
        }

        # Does it have characters other than letters?
        if ( ($userPrefix -match '\W') -or ($userPrefix -match '\d') ) {
            $isValidString = $false
        } else {
            $isValidString = $true
        }

        Write-Host "You have chosen $userPrefix as your user prefix.  Is this correct?  Y or N " -BackgroundColor $standardBackground -ForegroundColor $standardText
        $userQuestionResponse = Read-Host
        if ( $userQuestionResponse -match '[yY]' ) {
            $userApprovesPrefix = $true
        } else {
            $userApprovesPrefix = $false
        }

        # Has a valid user prefix been provided?
        if ( $isString -and $isValidString -and $userApprovesPrefix ) {
            $userPrefixIsInvalid = $false
        } else {
            $userPrefixIsInValid = $true
        }

        if (-not $isString ) {
            Write-Warning -Message "Input was not a string. Please try again."
        } elseif ( -not $isValidString ) {
            Write-Warning -Message "$userPrefix contains characters other than letters. Please try again."
        }

    } while ( $userPrefixIsInvalid )

    $userPrefix

}
function PasswordCreationInstructionsSingle {
    Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "#              Password Creation               #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# You have two  options for creating the user  #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# password:                                    #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "#                                              #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# [A] Enter a password.                        #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "#                                              #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# [B] Generate a random password.              #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText
    
    Write-Host "`nType A or B and press ENTER." -BackgroundColor $standardBackground -ForegroundColor $standardText

}

function PasswordCreationChoiceSingle {
    $isDataValid = $false
    $singlePasswordChoiceWarningMessage = "Invalid entry.  Please enter A or B."
    do {
        PasswordCreationInstructionsSingle

        $singlePasswordChoice = Read-Host

        switch -Regex ( $singlePasswordChoice ) {
            [Aa] { $isDataValid = $true }
            [Bb] { $isDataValid = $true }
            Default { Write-Warning -Message $singlePasswordChoiceWarningMessage }
        }
    } while ( -not $isDataValid )

    $singlePasswordChoice
}

function PasswordCreationInstructionsMultiple {
    Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "#              Password Creation               #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# You have three options for user password:    #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "#                                              #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# [A] Enter a password for all users.          #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "#                                              #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# [B] Generate a random password to be shared  #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# by all users.                                #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "#                                              #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# [C] Generate a random password unique for    #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "# each user.                                   #" -BackgroundColor $standardBackground -ForegroundColor $standardText
    Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText

    Write-Host "`nType A, B, or C, and press ENTER." -BackgroundColor $standardBackground -ForegroundColor $standardText
}

function PasswordCreationChoiceMultiple {
    $isDataValid = $false
    $warningMessage = "Invalid entry.  Please enter A, B, or C."

    do {
        PasswordCreationInstructionsMultiple

        $passwordChoice = Read-Host
        
        switch -Regex ($passwordChoice ) {
            [Aa] { $isDataValid = $true }
            [Bb] { $isDataValid = $true }
            [Cc] { $isDataValid = $true }
            Default { Write-Warning -Message $warningMessage }
        }
    } while ( -not $isDataValid )

    $passwordChoice
}

function PasswordUserSpecified {
    $passwordSpecificationWarningMessage = "Invalid entry. Please confirm with Y or N."
    do {
        Write-Host "`nType the desired password and press Enter." -BackgroundColor $standardBackground -ForegroundColor $standardText
        $capturePassword = Read-Host
        Write-Host "`nThe password you entered: $capturePassword" -BackgroundColor $standardBackground -ForegroundColor $standardText
        Write-Host "`nIs this correct?  Type Y or N, and press Enter." -BackgroundColor $standardBackground -ForegroundColor $standardText
        $passwordConfirmation = Read-Host

        switch -Regex ( $passwordConfirmation ) {
            [Yy] { $isDataValid = $true }
            [Nn] { $isDataValid = $false}
            Default { Write-Warning -Message $passwordSpecificationWarningMessage ;  $isDataValid = $false }
        }
    } while ( -not $isDataValid )
    $capturePassword
}

function PasswordRandomGeneration {
    # Thank you Reddit for the below idea and inspiration:  https://www.reddit.com/user/savemysettings/
    # Eventually I'll make this script more interactive with users choosing how many of each
    # character type.
    # Yes, I know [System.Web.Security.Membership] exists.  Currently not working on Linux.
    # I want to avoid dependencies like that for now.
    $passwordCharacterTypes = @{
        upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray()
        lowerCase = "abcdefghijklmnopqrstuvwxyz".ToCharArray()
        numbers = "1234567890".ToCharArray()
        symbols = '~!@#$%^&*_-+=|\(){}[]:;"<>,.?/'.ToCharArray()
    }

    $upperForPassword = Get-Random -Count 4 -InputObject $passwordCharacterTypes.upperCase
    $lowerForPassword = Get-Random -Count 4 -InputObject $passwordCharacterTypes.lowerCase
    $numberForPassword = Get-Random -Count 2 -InputObject $passwordCharacterTypes.numbers
    $symbolForPassword = Get-Random -Count 2 -InputObject $passwordCharacterTypes.symbols

    $totalPasswordCharacters = $upperForPassword + $lowerForPassword + $numberForPassword + $symbolForPassword
    $randomizePasswordCharacters = Get-Random -Count $totalPasswordCharacters.count -InputObject $totalPasswordCharacters
    $finalPasswordString = [string]::Concat($randomizePasswordCharacters)

    $finalPasswordString
}

# Start the work

Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText
Write-Host "# Welcome to the Test Account Creation Script! #" -BackgroundColor $standardBackground -ForegroundColor $standardText
Write-Host "#                                              #" -BackgroundColor $standardBackground -ForegroundColor $standardText
Write-Host "# It will create the desired number of users   #" -BackgroundColor $standardBackground -ForegroundColor $standardText
Write-Host "# in the users container based on a prefix     #" -BackgroundColor $standardBackground -ForegroundColor $standardText
Write-Host "# you provide, create a password, and export   #" -BackgroundColor $standardBackground -ForegroundColor $standardText
Write-Host "# the result.                                  #" -BackgroundColor $standardBackground -ForegroundColor $standardText
Write-Host "################################################" -BackgroundColor $standardBackground -ForegroundColor $standardText

# Step 0: Check for AD PowerShell module and domain admin credentials.

try {
    Import-Module -Name ActiveDirectory -ErrorAction Stop
}
catch {
    $Error[0]
    if ( -not (Get-Module -Name ActiveDirectory ) ) {
        Write-Error -Message "ActiveDirectory PowerShell module is missing.  Exiting script."
        Exit
    }
}

do {
    Write-Host "`nEnter domain administrator credentials." -BackgroundColor $standardBackground -ForegroundColor $standardText
    $domainAdminCredentials = Get-Credential

    if ( $domainAdminCredentials.UserName -match '@' ) {
        $domainAdminCredentialsSAM = $domainAdminCredentials.UserName.Split('@')[0]
    } else {
        $domainAdminCredentialsSAM = $domainAdminCredentials.UserName
    }

    try {
        Get-AdUser -Identity $domainAdminCredentialsSAM -Credential $domainAdminCredentials -ErrorAction Stop | Out-Null
        $isDomainCredentialValid = $true
    }
    catch [System.Security.Authentication.AuthenticationException] {
        $isDomainCredentialValid = $false
        Write-Warning -Message "Credential for $($domainAdminCredentials.username) is invalid."
    }
    
    $checkForDomainAdmin = Get-ADGroupMember -Identity 'Domain Admins' | Where-Object SamAccountName -match $domainAdminCredentialsSAM
    if ( $checkForDomainAdmin ) {
        $isDomainAdmin = $true
    } else {
        $isDomainAdmin = $false
        Write-Warning -Message "$($domainAdminCredentials.username) is not a domain administrator."
    }

    if ( $isDomainCredentialValid -and $isDomainAdmin ) {
        $isValidCredential = $true
    } else{
        $isValidCredential = $false
    }
} while ( -not $isValidCredential )

# Step 1:  Determine the number of accounts to create

$totalUsersToCreate = DetermineNumberOfUsersToCreate

# Step 2:  Determine prefix for user account names

$chosenUserPrefix = UserPrefixCreationChoice

# Step 3:  Create Passwords

if ( $totalUsersToCreate -eq 1 ) { ## For single user
    $singleUserPasswordCreationMethod = PasswordCreationChoiceSingle

    switch -Regex ( $singleUserPasswordCreationMethod ) {
        [Aa] { $userPassword = PasswordUserSpecified }
        [Bb] { $userPassword = PasswordRandomGeneration }
    }
}

if ( $totalUserstocreate -gt 1 ) { ## For multiple users
    $multipleUserPasswordCreationMethod = PasswordCreationChoiceMultiple

    switch -Regex ( $multipleUserPasswordCreationMethod ) {
        [Aa] { $userPassword = PasswordUserSpecified }
        [Bb] { $userPassword = PasswordRandomGeneration }
        [Cc] { $userPasswordArray = @()
                for ( $i = 0; $i -le $totalUsersToCreate; $i++ ) {
                    $userPasswordArray += PasswordRandomGeneration
                }
            }
    }
}

# Step 4:  Prepare User Objects

$domainInformation = Get-ADDomain
$usersContainer = $domainInformation.UsersContainer
$domainFQDN = $domainInformation.DNSRoot

$userObjectsForCreation = @()

if ( $totalUsersToCreate -eq 1 ) { # For single users
    $ind = [PSCustomObject]@{
        Name = $chosenUserPrefix
        GivenName = $chosenUserPrefix
        DisplayName = $chosenUserPrefix
        SamAccountName = $chosenUserPrefix
        AccountPassword = $userPassword
        UserPrincipalName = "$chosenUserPrefix@$domainFQDN"
        CreationPath = $usersContainer
    }
    $userObjectsForCreation += $ind
}

if ( ($totalUsersToCreate -gt 1) -and ( $null -ne $userPassword) ){ # For multiple users with single password
    for ( $i = 1 ; $i -le $totalUsersToCreate ; $i++ ) {
        $ind = [PSCustomObject]@{
            Name = $chosenUserPrefix + [String]$i
            GivenName = $chosenUserPrefix + [String]$i
            DisplayName = $chosenUserPrefix + [String]$i
            SamAccountName = $chosenUserPrefix + [String]$i
            AccountPassword = $userPassword
            UserPrincipalName = $chosenUserPrefix + [String]$i + "@$domainFQDN"
            CreationPath = $usersContainer
        }
        $userObjectsForCreation += $ind
    }
} elseif ( $totalUsersToCreate -gt 1 ){ # For multiple users with multiple passwords
    for ( $i = 1 ; $i -le $totalUsersToCreate.count ; $i++ ) {
        $ind = [PSCustomObject]@{
            Name = $chosenUserPrefix + [String]$i
            GivenName = $chosenUserPrefix + [String]$i
            DisplayName = $chosenUserPrefix + [String]$i
            SamAccountName = $chosenUserPrefix + [String]$i
            AccountPassword = $userPasswordArray[$1-1]
            UserPrincipalName = $chosenUserPrefix + [String]$i + "@$domainFQDN"
            CreationPath = $usersContainer
        }
        $userObjectsForCreation += $ind
    }
}

# Step 5:  Check to see if the users already exist.

foreach ( $userCheck in $userObjectsForCreation ) {
    Remove-Variable -Name checkFilter -ErrorAction SilentlyContinue
    $checkFilter = "SamAccountName -eq '$($userCheck.SamAccountName)'"
    if ( (Get-ADUser -filter $checkFilter) ) {
        Write-Error -Message " $($userCheck.SamAccountName) already exists.  Exiting script.  No users have been created."
        Exit
    }
}

# Step 6:  Create Users in AD

foreach ( $a in $userObjectsForCreation ) {

    try {
        New-ADUser -Name $a.Name -DisplayName $a.DisplayName -GivenName $a.GivenName `
        -SamAccountName $a.SamAccountName -UserPrincipalName $a.UserPrincipalName `
        -AccountPassword (ConvertTo-SecureString -String $a.AccountPassword -AsPlainText -Force) `
        -ChangePasswordAtLogon $false -Enabled $true -Credential $domainAdminCredentials
    }
    catch [Microsoft.ActiveDirectory.Management.ADPasswordComplexityException] {
        Write-Warning "Password for $($a.name) did not meet you domain's complexity requirements. User created in a disabled state."
             
    }
    
}

# Step 7:  Export user info CSV

$userObjectsForCreation | Export-Csv -NoTypeInformation -Path ".\GeneratedUserDetails$(Get-Date -Format yyyyMMddThhmmss).csv"


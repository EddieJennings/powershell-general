<#
.SYNOPSIS
    Create DNS recrods from CSV file

.DESCRIPTION
    Create DNS records from CSV file.
    The account that runs this script must have privleges to access Windows DNS Server.
    The necessary DNS zones must be created prior to running the script.
    The script checks to see if A records or PTR records already exist.
    If they do, the script will not try to create a new record or modify
    the existing record.  It will make a note in the report file.

.NOTES
    Author:  Eddie Jennings, Jr
    Last Modified:  2021-03-23
#>

#####
# IMPORTANT:  CSV File Information
#
# CSV file must be named DNSImport.csv and must be located in the same directory as this script.
# CSV file must have the following headings:  IPAddress,HostName,ZoneName
#
# Sample CSV file would look like this for adding the records foo.bar.com and blah.bar.com
# IPAddress,HostName,ZoneName
# 1.1.1.1,foo,bar.com
# 1.1.1.2,blah,bar.com
#####

$importFile = (Get-Location).Path + "\DNSImport.csv"
$resultsFile = (Get-Location).Path + "\DNSImportResults.csv"

if ( -not (Test-Path -Path $importFile) ){
    Write-Error -Message "Missing import file: $importFile" -ErrorAction Stop
}

$dnsRecordsToBeCreated = Import-CSV -Path $importFile
$ARecordAlreadyExistsMessage = "An A record already exists. "
$PTRRecordAlreadyExistsMessage = "A PTR record already exists. "
$dnsRecordCreationResults = @()

foreach ( $i in $dnsRecordsToBeCreated ) {

# Set Variables for each loop iteration
    $ARecordCreated = $null
    $ARecordAlreadyExists = $null
    $PTRRecordCreated = $null
    $PTRRecordAlreadyExists = $null

    $forwardZoneName = $i.ZoneName
    $forwardZoneHost = $i.HostName
    $forwardZoneRecord = $i.IPAddress
    $initialARecordCheck = Get-DnsServerResourceRecord -ZoneName $forwardZoneName -Name $forwardZoneHost -RRType A -ErrorAction SilentlyContinue

    $reverseZoneName = "$($i.IPAddress.Split("{.}")[2]).$($i.IPAddress.Split("{.}")[1]).$($i.IPAddress.Split("{.}")[0]).in-addr.arpa"
    $reverseZoneHost = $i.IPAddress.Split("{.}")[3]
    $reverseZoneRecord = "$forwardZoneHost.$forwardZoneName."
    $PTRDomain = "$forwardZoneHost.$forwardZoneName"
    $initialPTRRecordCheck = Get-DnsServerResourceRecord -ZoneName $reverseZoneName -Name $reverseZoneHost -RRType PTR -ErrorAction SilentlyContinue | `
        Where-Object { $PSItem.RecordData.PtrDomainName -eq $reverseZoneRecord }

# Creating A Record

    if ( -not $initialARecordCheck ){
        Add-DnsServerResourceRecordA -ZoneName $forwardZoneName -Name $forwardZoneHost -IPv4Address $forwardZoneRecord
        $ARecordCheck = Get-DnsServerResourceRecord -ZoneName $forwardZoneName -Name $forwardZoneHost -RRType A -ErrorAction SilentlyContinue
        if ( $ARecordCheck ){
            $ARecordCreated = $true
        }
        else {
            $ARecordCreated = $false
        } #nested if/else
    }
    else {
        $ARecordCreated = $false
        $ARecordAlreadyExists = $true
    } #original if/else

# Creating PTR Record

    if ( -not $initialPTRRecordCheck ){
        Add-DnsServerResourceRecordPtr -ZoneName $reverseZoneName -Name $reverseZoneHost -PtrDomainName $PTRDomain
        $PTRRecordCheck = Get-DnsServerResourceRecord -ZoneName $reverseZoneName -Name $reverseZoneHost -RRType PTR -ErrorAction SilentlyContinue | `
        Where-Object { $PSItem.RecordData.PtrDomainName -eq "$PTRDomain." }
        if ( $PTRRecordCheck ){
            $PTRRecordCreated = $true
        }
        else {
            $PTRRecordCreated = $false
        } #nested if/else
    }
    else {
        $PTRRecordCreated = $false
        $PTRRecordAlreadyExists = $true
    } #original if/else

# Set the value of Notes

    $notes = $null
    if ( $ARecordAlreadyExists ) {
        $notes = $ARecordAlreadyExistsMessage
    }
    if ( $PTRRecordAlreadyExists ) {
        $notes = $notes + $PTRRecordAlreadyExistsMessage
    }

    if ( $ARecordAlreadyExists -or $PTRRecordAlreadyExists ) {
        $notes = $notes + "No change made."
    }

# Create Results Object    

    $indRecordCreationResults = [PSCustomObject]@{
        IPAddress = $i.IPAddress
        FQDN = "$($i.HostName).$($i.ZoneName)"
        ARecordCreated = $ARecordCreated
        PTRRecordCreated = $PTRRecordCreated
        Notes = $notes
    }

    $dnsRecordCreationResults += $indRecordCreationResults

} #for i

# Export Results Object

$dnsRecordCreationResults | Sort-Object -Property IPAddress | Export-Csv -NoTypeInformation -Path $resultsFile

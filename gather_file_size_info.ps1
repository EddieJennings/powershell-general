<#
.SYNOPSIS
	Create a CSV of files in a directory tree and show relevant information.
.DESCRIPTION
	Use this script to generate a list of all files in a directory structure,
	which shows the name, directory, file size in gibibytes, the NTFS owner,
	and last write time.

	Author:  Eddie Jennings, Jr.
	E-mail:  eddie@eddiejennings.net
	Modified: 2021-06-14
#>

### SET THESE VARIABLES BEFORE RUNNING

$CheckPath = "Your:\Desired\Directory" # Where are you looking?
$CSVFile = "Your:\Desired\File.csv" # What is the path of the CSV file

# Start the work

$allFiles = Get-ChildItem -Path $CheckPath -File -Recurse | Select-Object -Property name, `
DirectoryName, FullName, @{n="SizeInGB"; e={($PSItem.Length)/1024/1024/1024}}, `
@{n="Owner"; e={(Get-Acl -Path $PSItem.FullName).Owner}}, LastWriteTime

$allFiles | Export-CSV -NoTypeInformation -Path $CSVFile

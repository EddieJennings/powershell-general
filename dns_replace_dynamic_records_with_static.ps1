<#
.SYNOPSIS
    Replace dynamic DNS A and PTR records with static records.

.DESCRIPTION
    From a list of hosts, delete their dynamic records and
    create static records with the same value.

    Before running, specify zoneName and and DNSServer variables.
    Make sure there is a file named hosts.txt in the $depsPath directory.  See $recordsToChange variable for more info.
    
    ###########################################
    # WARNING: THIS IS AN INTERACTIVE SCRIPT! #
    # Do not use for a scheduled task!        #
    ###########################################

.NOTES
    Author:  Eddie Jennings, Jr
#>

# Set variables

$DNSServer = 'YOUR_WINDOWS_DNS_SERVER_HOSTNAME' # change to your DNS host name
$logfile = ".\convert_dynamic_dns_files\results$(Get-Date -format yyyyMMddThhmmss).log"
$zoneName = 'YOUR_DOMAIN.TLD' # change to the forward lookup zone you're targeting
$depsPath = ".\convert_dynamic_dns_files"
$hostsFile = "$depsPath\hosts.txt"
$reverseZones = Get-DnsServerZone -ComputerName $DNSServer | Where-Object -Property IsReverseLookupZone -eq $true
$ptrRecords = @()

# Require user confirmation before continuing
Write-Warning -Message "This script must be run as an account that can connect to the $DNSServer DNS server else you may receive undesired results."
Read-Host -Prompt 'Press ENTER to continue or CTRL+C to quit.'

# Check for dependencies

if ( -not (Test-Path -Path $hostsfile) ) {
    Write-Error -Message "$hostsFile not found.  Aborting script."
    Exit
}

$recordsToChange = Get-Content -Path $hostsFile # text file of the name portion of the A record, not the FQDN

# Create log
if ( -not (Test-Path -Path $logfile) ) {
    New-Item -Path $logfile -ItemType File
}

# Gather PTR records
foreach ( $i in $reverseZones ) {
    $ptrRecords += Get-DnsServerResourceRecord -ZoneName $i.ZoneName -RRType Ptr -ComputerName $DNSServer
}

# Start the work
foreach ( $j in $recordsToChange ) {
    $dynamicARecord = Get-DnsServerResourceRecord -ZoneName $zoneName -RRType A -Name $j -ComputerName $DNSServer | Where-Object { $null -ne $PSItem.TimeStamp }
    $PTRName = "$j.$zoneName."
    $dynamicPTRRecord = $ptrRecords | Where-Object { ($PSItem.RecordData.PtrDomainName -eq $PTRName) -and ($null -ne $PSItem.TimeStamp) }

    # Work with A records

    if ( ($dynamicARecord | Measure-Object).Count -eq 1 ) {
        $ipAddress = $dynamicARecord.RecordData.IPv4Address.IPAddressToString
        Remove-DnsServerResourceRecord -Name $j -RRType A -ZoneName $zoneName -ComputerName $DNSServer -Force
        Add-DnsServerResourceRecordA -ZoneName $zoneName -Name $j -IPv4Address $ipAddress -ComputerName $DNSServer
        $testForARecordSuccess = $true
    } elseif ( ($dynamicARecord | Measure-Object).Count -lt 1 ) {
        Add-Content -Path $logfile -Value "$j.$zoneName does not have a dynamic A record"
        $testForARecordSuccess = $false
    } else {
        Add-Content -Path $logfile -Value "$j.$zoneName has more than one dynamic A record.  Manual review required."
        $testForARecordSuccess = $false
    } #end if

    # Log A record creation results

    if ( $testForARecordSuccess ) {
        $checkForARecordSuccess = Get-DnsServerResourceRecord -Name $j -RRType A -ZoneName $zoneName -ComputerName $DNSServer
        if ( $checkForARecordSuccess -and ($null -eq $checkForARecordSuccess.TimeStamp) ) {
            Add-Content -Path $logfile -Value "$j.$zoneName static A record creation succeeded."
        } else {
            Add-Content -Path $logfile -Value "$j.$zoneName static A record creation failed. Manual intervention required to create an A record with IP address $ipAddress"
        } #end inner if
    } #end if

    # Work with PTR records

    if ( ($dynamicPTRRecord | Measure-Object).Count -eq 1 ) {
        $DNParts = $dynamicPTRRecord.DistinguishedName.Split(',')
        $ZoneParts = $DNParts[1].Split('=')
        $PTRZone = $ZoneParts[1]
        Remove-DnsServerResourceRecord -Name $dynamicPTRRecord.HostName -ZoneName $PTRZone -RRType Ptr -ComputerName $DNSServer -Force
        Add-DnsServerResourceRecordPtr -Name $dynamicPTRRecord.HostName -ZoneName $PTRZone -PtrDomainName $PTRName -ComputerName $DNSServer
        $testForPTRRecordSuccess = $true
    } elseif ( ($dynamicPTRRecord | Measure-Object).Count -lt 1 ) {
        Add-Content -Path $logfile -Value "$PTRName does not have a dynamic PTR record."
        $testForPTRRecordSuccess = $false
    } else {
        Add-Content -Path $logfile -Value "$PTRName has more than one dynamic PTR record. Manual review required."
        $testForPTRRecordSuccess = $false
    } #end if

    # Log PTR record creation results

    if ( $testForPTRRecordSuccess ) {
        $checkforPTRRecordSuccess = Get-DNSServerResourceRecord -ZoneName $PTRZone -Name $dynamicPTRRecord.HostName -RRType Ptr -ComputerName $DNSServer
        if ( $checkforPTRRecordSuccess -and ($null -eq $checkforPTRRecordSuccess.TimeStamp) ) {
            Add-Content -Path $logfile -Value "$($dynamicPTRRecord.HostName).$PTRZone static PTR record creation succeeded."
        } else {
            Add-Content -Path $logfile -Value "$($dynamicPTRRecord.HostName).$PTRZone static PTR record creation failed.  Manual intervention required to create a PTR record for host $PTRName"
        } #end inner if
    } #end if
    
} #end for

Write-Verbose -Message "Script complete.  Check $logfile for more information. " -Verbose

<#
.SYNOPSIS
    Gather all PTR records for a particular IP address
.DESCRIPTION
    Requires a text file named ipaddresses.txt to be present in the directory in
    which this script is run.  Format of the text file should be as follows:
    #.#.#.#
    #.#.#.#

    Author:  Eddie Jennings, Jr.
    Modified:  2021-03-22
#>

[CmdletBinding()]
Param()

$importFile = (Get-Location).Path + "\ipaddresses.txt"
$exportFile = (Get-Location).Path + "\PTRrecords.csv"
$zoneSuffix="in-addr.arpa"

# Check for existence of import file.  Stop script if missing.

if ( -not (Test-Path -Path $importFile) ) {
    Write-Error -Message "Missing import file: $importFile" -ErrorAction Stop
}

# Convert IP addresses to objects

$originalAddresses = Get-Content -Path ./ipaddresses.txt

$allIPAddressObjects = @()

foreach ( $i in $originalAddresses ) {
    $indIP = [PSCustomObject]@{
        FirstOctet = $i.Split("{.}")[0]
        SecondOctet = $i.Split("{.}")[1]
        ThirdOctet = $i.Split("{.}")[2]
        FourthOctet = $i.Split("{.}")[3]
        FullAddress = $i

    }

    $allIPAddressObjects += $indIP

} # foreach original addresses

# Determine DNS Zones to check

Write-Verbose -Message "Determining DNS Zones to Check."

$allDNSZones = @()

foreach ( $j in $allIPAddressObjects ) {
    $allDNSZones += "$($j.ThirdOctet).$($j.SecondOctet).$($j.FirstOctet).$zoneSuffix"

} # foreach j

$DNSZonesToCheck = $allDNSZones | Select-Object -Unique

# Gather PTR records

Write-Verbose -Message "Gathering PTR records."

$ipPtrRecords = @()

foreach ( $k in $DNSZonesToCheck ) {
    Remove-Variable -Name ipAddressesInZone,allPtrRecords -ErrorAction SilentlyContinue

    $ipAddressesInZone = $allIPAddressObjects | Where-Object { ($PSItem.FirstOctet -eq $k.Split("{.}")[2]) -and ($PSItem.SecondOctet -eq $k.Split("{.}")[1]) -and ($PSItem.ThirdOctet -eq $k.Split("{.}")[0]) }
    
    if ( Get-DnsServerZone -Name $k -ErrorAction SilentlyContinue ) {
        $allPtrRecords = Get-DnsServerResourceRecord -ZoneName $k -RRType Ptr
        foreach ( $l in $ipAddressesInZone ) {
            Remove-Variable -Name indPtrRecords,recordCounter -ErrorAction SilentlyContinue
            $indPtrRecords = $allPtrRecords | Where-Object { $PSItem.HostName -eq $l.FourthOctet }
            $recordCounter = ($indPtrRecords | Measure-Object).Count
            while ( $recordCounter -gt 0 ) {
                $recordArrayPosition = $recordCounter - 1
                $indRecord = [PSCustomObject]@{
                    IPAddress = $l.FullAddress
                    PTRRecord = $indPtrRecords[$recordArrayPosition].RecordData.PtrDomainName
                }
                $ipPtrRecords += $indRecord
                $recordCounter--
            } # while recordcounter
        } # foreach l
    } # if
        else {
            foreach ( $m in $ipAddressesInZone ) {
                $errorRecord = [PSCustomObject]@{
                    IPAddress = $m.FullAddress
                    PTRRecord = "No PTR Record found"
                }
                $ipPtrRecords += $errorRecord
            } # foreach m
        } # else
} # foreach k

# Export PTR Records

Write-Verbose -Message "Exporting PTR records to $exportFile."

$ipPtrRecords | Sort-Object -Property IPAddress | Export-Csv -NoTypeInformation -Path $exportFile

Write-Verbose -Message "Script complete."

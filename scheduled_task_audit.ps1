<#

.SYNPOSIS
    Audit all scheduled tasks.
.DESCRIPTION
    This script audits all scheduled tasks configured on this server.
    Reports are E-mailed to interested recipients.

    Author: Eddie Jennings, Jr.
    E-mail: eddie@eddiejennings.net
    Modified: 2021-07-09
#>

# Note:  You will need to change variables to work with your environment

$senderAddress = 'mailboxname@domain.tld'
$recipientAddress = "mailboxname@domain.tld"
$messageSubject = "Windows Task Scheduler Server Report"
$SMPTServer = 'your_smtp_server'
$tempDir = 'C:\path\to\the\temp\dir\you\want\to\use' # Where reports and email body text are stored
$messageBodyTempFile = "$tempDir\msg_body_tmp.txt"
$taskAccount = "DOMAIN\username"

$allScheduledTasks = Get-ScheduledTask -TaskPath "\*"
$scheduledTaskInformation = @()

# Gather facts for the scheduled tasks

foreach ( $i in $allScheduledTasks ) {

    $indTaskName = $i.TaskName
    $indTaskPath = $i.TaskPath
    $indTaskAuthor = ($i.CimInstanceProperties | Where-Object -Property Name -eq "Author").Value
    $indTaskAccount = ($i.CimInstanceProperties | Where-Object -Property Name -eq "Principal").Value.UserID
    $indTaskState = $i.State
    $indTaskCreationDate = $i.Date
    $indTaskDescription = ($i.CimInstanceProperties | Where-Object -Property Name -eq "Description").Value

    $indTaskInfo = [PSCustomObject] @{
        TaskName = $indTaskName
        TaskPath = $indTaskPath
        TaskState = $indTaskState
        TaskAuthor = $indTaskAuthor
        TaskRunningAccount = $indTaskAccount
        TaskCreationDate = $indTaskCreationDate
        TaskDescription = $indTaskDescription
        }

    $scheduledTaskInformation += $indTaskInfo

}

# Make temporary directory if necessary

if ( -not (Test-Path -Path $tempDir) ) {
    New-Item -Path $tempDir -ItemType Directory
}

# Create Scheduled task CSV report
$reportName = "ScheduledTaskReport_$(Get-Date -Format yyyyMMdd).csv"
$reportPath = "$tempDir\$reportName"

$scheduledTaskInformation | Sort-Object -Property TaskName | Export-Csv -NoTypeInformation -Path $reportPath -Force

# Filter to include only a certain task directory

$CertainTasks = $scheduledTaskInformation | Where-Object -Property TaskPath -Like "\your_path\*"

# Find certain tasks not configured to use designed task account

$CertainTasksNotUsingTaskAccount = $CertainTasks | Where-Object -FilterScript { ($PSItem.TaskRunningAccount -ne $taskAccount) -and ($PSItem.TaskRunningAccount -ne $taskAccount) }

# Filter to include only non-Certain-Tasks tasks

$NonCertainTasks = $scheduledTaskInformation | Where-Object -Property TaskPath -NotLike "\your_path\*"

# Find non-certain tasks configured to use the task accounts.

$NonCertainTaskUsingCSIRCTaskAccounts = $NonCertainTasks | Where-Object -FilterScript { ($PSItem.TaskRunningAccount -eq $taskAccount) }

# Create the E-mail message body by appending to a temporary text file.

$noTasksMessage = "There are no scheduled tasks meeting these criteria."

Set-Content -Path $messageBodyTempFile -Value "This E-mail contains a report of all scheduled tasks configured on $($env:COMPUTERNAME) as well as other relevant information."
Add-Content -Path $messageBodyTempFile -Value ""
Add-Content -Path $messageBodyTempFile -Value "######"
Add-Content -Path $messageBodyTempFile -Value "These tasks within the certain tasks folder are not configured to use the designated task accounts."
Add-Content -Path $messageBodyTempFile -Value "######"
Add-Content -Path $messageBodyTempFile -Value ""
if ( $CertainTasksNotUsingTaskAccount ) {
    Add-Content -Path $messageBodyTempFile -Value ($CertainTasksNotUsingTaskAccount | Select-Object -Property TaskName,TaskPath,TaskState,TaskAuthor | Out-String)
} else {
    Add-Content -Path $messageBodyTempFile -Value $noTasksMessage
  }
Add-Content -Path $messageBodyTempFile -Value ""
Add-Content -Path $messageBodyTempFile -Value "######"
Add-Content -Path $messageBodyTempFile -Value "These tasks are not within the certain tasks folder but are configured to use the designated task account."
Add-Content -Path $messageBodyTempFile -Value "######"
Add-Content -Path $messageBodyTempFile -Value ""
if ( $NonCertainTaskUsingCSIRCTaskAccounts ) {
    Add-Content -Path $messageBodyTempFile -Value ($NonCertainTaskUsingCSIRCTaskAccounts | Select-Object -Property TaskName,TaskPath,TaskState,TaskAuthor | Out-String)
} else {
    Add-Content -Path $messageBodyTempFile -Value $noTasksMessage
  }
Add-Content -Path $messageBodyTempFile -Value ""
Add-Content -Path $messageBodyTempFile -Value "######"
Add-Content -Path $messageBodyTempFile -Value ""
Add-Content -Path $messageBodyTempFile -Value "See the attached file, $reportName, for a listing of all scheduled tasks configured on the server."

# Generate the E-mail

Send-MailMessage -From $senderAddress -To $recipientAddress -Subject $messageSubject -Body (Get-Content -Path $messageBodyTempFile | Out-String) -Attachments $reportPath -SmtpServer $SMPTServer

# Cleanup temporary directory

Get-ChildItem -Path $tempDir | Remove-Item -Force

# TODO
# - modularize stuff
# - tweak for more general usage

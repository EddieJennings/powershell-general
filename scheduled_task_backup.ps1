﻿<#
.SYNOPSIS
    Export Scheduled Tasks for Backup

.DESCRIPTION
    This script will export scheduled tasks, and retain 30 days of backups.

.NOTES
    Author:  Eddie Jennings, Jr.
    Email: eddie@eddiejennings.net
    Last Modified: 2021-07-01
#>

# Gather CSIRC scheduled tasks

$allScheduledTasks = Get-ScheduledTask -TaskPath \YourRootFolder\* # \* is important if you want to get scheduled tasks recursively
$backupRoot = "C:\backup" # Set to wherever you want backups to be stored
$backupRetention = 30 # number of days

# Create backup directory

$backupDir = "$backupRoot\$(Get-Date -Format yyyymmdd_HHMMss)"
if ( -not (Test-Path -Path $backupDir) ) {
    New-Item -Path $backupDir -ItemType Directory
}

# Export CSIRC scheduled tasks

foreach ( $i in $allScheduledTasks ) {
    $taskBackupDir = $backupDir + $i.TaskPath
    if ( -not (Test-Path -Path $taskBackupDir) ) {
        New-Item -Path $taskBackupDir -ItemType Directory
    }
    $taskBackupName = $taskBackupDir + $i.TaskName + ".xml"
    Export-ScheduledTask -TaskName $i.TaskName -TaskPath $i.TaskPath | Out-File -FilePath $taskBackupName
}

# Cleanup backups per retention requirement

$deletionCandidates = Get-ChildItem -Path $backupRoot -Directory | Where-Object {$PSItem.CreationTime -lt (Get-Date).AddDays(-$backupRetention)}
$deletionCandidates | Remove-Item -Recurse -Force

# TO DO LIST
# robocopy to the remote location
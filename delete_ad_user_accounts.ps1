<#
.SYNOPSIS
    Delete AD user accounts as well as remove group membership.

.DESCRIPTION
    Delete AD user accounts as well as remove group membership.

.PARAMETER UserID
    This can be a SAM Account Name or ObjectGUID

.PARAMETER UserList
    This must be a list (one entry per line) of SAM Account Names or ObjectGUIDS

.EXAMPLE
    For a single user

    ./deleteADUserAccounts.ps1 -UserID some_SAM_or_ObjectGUID

    For multiple users

    ./deleteADUserAccounts.ps1 -UserList "C:\Path\To\Your\list.txt"

.NOTES
    Author:  Eddie Jennings, Jr.
#>

# Establish parameters

[CmdletBinding()]
param (

    [Parameter(Mandatory=$false)]
    [String]$UserID,
    [Parameter(Mandatory=$false)]
    [String]$UserList

)

# Check for valid parameters
if ( ($null -eq $userID) -and ($null -eq $UserList) ) {
    Write-Error -Message "You must specify the -UserID or the -UserList parameters"
    Exit
}
elseif ( $UserID -and $UserList ) {
    Write-Error -Message "You cannot specify both -UserID and -UserList parameters."
    Exit
}

# For deleting single users

if ( $UserID ) {
    $singleUserGroups = Get-ADPrincipalGroupMembership -Identity $UserID
    
    foreach ( $singlegroup in $singleUserGroups ) {
        Remove-ADGroupMember -Identity $singlegroup.objectGUID -Members $UserID -Confirm:$false -Verbose
    }

    Remove-ADUser -Identity $UserID -Confirm:$false -Verbose
}

# For deleting multiple users

if ( $UserList ) {
    $usersForDeletion = Get-Content -Path $UserList

    foreach ( $user in $UsersForDeletion ) {
        $groupMembership = Get-ADPrincipalGroupMembership -Identity $user

        foreach ( $usergroup in $groupMembership ) {
            Remove-ADGroupMember -Identity $usergroup.objectGUID -Members $user -Confirm:$false -Verbose
        } # end foreach usergroup

        Remove-ADUser -Identity $user -Confirm:$false -Verbose
    } #end foreach user
} #end if

<#
.SYNOPSIS
    Gather client machine network interface info for IPv6 DHCP reservations.

.DESCRIPTION
    Gather client machine network interface info for IPv6 DHCP reservations.

.NOTES
    Author:  Eddie Jennings, Jr
    E-mail:  eddie@eddiejennings.net
#>
$exportFile = "C:\users\$env:USERNAME\Desktop\ipv6NICInfo-$env:COMPUTERNAME.csv"
$emailaddress = "youremail@domain.tld"

Write-Verbose -Message "Gathering IPv6 DUID" -Verbose

$DUID = Get-ItemPropertyValue -Path HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip6\Parameters -Name Dhcpv6DUID
$maxDUID = $DUID.count
$countDUID = 1
$duidAsString = $null
foreach ($id in $DUID) {
    $duidAsString += ([System.Convert]::ToString($id,16)).PadLeft(2,'0')
    if ($countDUID -lt $maxDUID) {
        $duidAsString += '-'
    }
    $countDUID++
}

Write-Verbose -Message "Gathering IPv6 IAIDs" -Verbose
Write-Warning -Message "'Property DHCPv6Iaid does not exist at path' error can be safely ignored." 


$ipv6InterfaceInfo = @()
$allNetAdapters = Get-NetAdapter
foreach ($nic in $allNetAdapters) {
    Remove-Variable -Name iaid,ind -ErrorAction SilentlyContinue
    $iaid = Get-ItemPropertyValue -Path "HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip6\Parameters\Interfaces\$($nic.InterfaceGUID)\" -Name DHCPv6Iaid -ErrorAction SilentlyContinue
        $ind = [PSCustomObject]@{
        InterfaceName = $nic.Name
        InterfaceIndex = $nic.InterfaceIndex
        InterfaceGUID = $nic.InterfaceGUID
        ComputerDUID = $duidAsString
        InterfaceIAID = $iaid
        InterfaceDescription = $nic.InterfaceDescription
        InterfaceIPv4MAC = $nic.MacAddress
        }
    $ipv6InterfaceInfo += $ind
}

Write-Verbose -Message "Exporting information to $exportFile." -Verbose

$ipv6InterfaceInfo | Export-Csv -NoTypeInformation -Path $exportFile

Write-Verbose -Message "Please E-mail the CSV file to $emailaddress.  Thank you!" -Verbose

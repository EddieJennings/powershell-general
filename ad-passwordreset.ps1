<#
.Description
Single-use interactive AD password reset

.Notes
	Author:  Eddie Jennings, Jr.
#>

$samAccountName = Read-Host "Enter the AD username for password reset"

Set-ADAccountPassword -Identity $samAccountName -NewPassword (Read-Host "Enter temporary AD password" -AsSecureString) -Reset

Set-ADUser -Identity $samAccountName -ChangePasswordAtLogon:$true

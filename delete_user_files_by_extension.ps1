<#
.DESCRIPTION
Delete files with the specified file extensions that have not been modified in X days.

For best effect, script should be run as an administrator

.NOTES
	Author:  Eddie Jennings, Jr. <eddie@eddiejennings.net>

.EXAMPLE
.\delete_user_files_by_extension.ps1 -FileExtension txt -SearchPath C:\users -MinDaysSinceLastFileMod 7

Deletes all .txt files within the folder tree C:\users (searches recursively) that have not been modified in the last week

.EXAMPLE
.\delete_user_files_by_extension.ps1 -FileExtension txt -SearchPath C:\users -MinDaysSinceLastFileMod 7 -ReportOnly

Output a list of all .txt files within the folder tree C:\users (searches recursively) that have not been modified in the last week.
No files are deleted with -ReportOnly

.EXAMPLE
.\delete_user_files_by_extension.ps1 -FileExtension txt

Uses only required parameter of -FileExtension
Deletes all .txt files within the folder tree of (and including) your current directory (searches recursively).

.EXAMPLE
.\delete_user_files_by_extension.ps1 -FileExtension txt,pdf,xlsx -SearchPath C:\users -MinDaysSinceLastFileMod 7

Deletes all .txt, .pdf, and .xlsx files within the folder tree C:\users (searches recursively) that have not been modified in the last week

.EXAMPLE
.\delete_user_files_by_extension.ps1 -FileExtension .txt,.pdf,.xlsx -SearchPath C:\users -MinDaysSinceLastFileMod 7

Deletes all .txt, .pdf, and .xlsx files within the folder tree C:\users (searches recursively) that have not been modified in the last week.
-FileExtension parameter accepts file extensions with or without the preceding dot.

#>

[cmdletbinding()]
Param(
    [Parameter(Position=0,
        Mandatory=$True)]
        [String[]]$FileExtension,
    [Parameter()]
        [string]$SearchPath,
    [Parameter()]
        [Int]$MinDaysSinceLastFileMod,
    [Parameter()]
        [switch]$ReportOnly
)

# Set and declare vars

$extToDelete = @()

# Populate $extToDelete
foreach ( $extension in $FileExtension ) {
    if ( $extension.StartsWith('.') ) {
        $extToDelete += $extension
    } else {
        $extToDelete += '.' + $extension
    }  
} #endfor extension

# Set $pathToSearch
if ( $SearchPath.Length -le 0 ) {
    $pathToSearch = (Get-Location).Path
} else {
    $pathToSearch = $SearchPath
}

# Set $days
if ( $MinDaysSinceLastFileMod -ge 0) {
    $days = -$MinDaysSinceLastFileMod # Turn user input into a negative number
} else {
    Write-Error -Message "Enter a non-negative interger for MinDaysSinceLastFileMod"
    Exit
}

# set $verboseSwitch
if ( $VerbosePreference -eq 'Continue' ) {
    $verboseSwitch = $true
} else {
    $verboseSwitch = $false
}

# Gather file deletion candidates

Write-Verbose -Message "Finding files for deletion in the $pathToSearch directory tree."
$filesToDelete += Get-ChildItem -Path $pathToSearch -Attributes !Directory -Recurse | Where-Object { ( $PSItem.LastWriteTime -lt (Get-Date).AddDays($days) ) -and ( $extToDelete -contains $PSItem.Extension ) }

# Report files if report mode is on otherwise delete files

if ( $ReportOnly.IsPresent ) {
    Write-Verbose -Message "These are the potential files for deletion."
    $filesToDelete | Select-Object -Property FullName,LastWriteTime | Sort-Object -Property FullName
    } else {
        Write-Verbose -Message "Deleting files."
        $filesToDelete | Remove-Item -Force -Verbose:$verboseSwitch
    }

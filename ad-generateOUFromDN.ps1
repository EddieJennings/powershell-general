<#
.SYNOPSIS
    Output the OU for an AD Object.

.DESCRIPTION
    Stores the OU path for an AD object in a variable.

.NOTES
    Author:  Eddie Jennings, Jr
    Last Modified:  2021-08-11
#>

$targetUsers = Get-ADUser -Identity your_identity # Set this to whatever you need for a user

foreach ( $i in $targetUsers ) {
    Remove-Variable -Name OUpieces,counter,OU -ErrorAction SilentlyContinue
    $OUpieces = $i.DistinguishedName.Split(',')
    $counter = 1
    $OU = $null
    while ( $counter -lt $OUPieces.count ) {
        $OU += $OUpieces[$counter]
        if ( $counter -lt ($OUpieces.count -1) ) {
            $OU += ','
        }
        $counter++
    }
    Write-Verbose -Message "The OU for $($i.SamAccountName) is $OU" -Verbose
}

# To do:
# - Make this into a function
# - Rework with Get-ADObject so it can be used for more than just user objects
